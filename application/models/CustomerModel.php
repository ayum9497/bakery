<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerModel extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
    }
    public function store($data)
    {
        return $this->db->insert('customer',$data);
    }
    public function show()
    {
        $this->db->order_by('created_at', 'DESC');
        $query=$this->db->get('customer');  
        return $query->result();  
    }
    public function details($id)
    {
        $query = $this->db->get_where('customer', array('cust_id' => $id));
		return $query->result(); 
    }
    public function delete($id)
    {
        $this->db->delete('orderbake', array('custID' =>$id));
        $this->db->delete('customer', array('cust_id' =>$id));
    }
    public function update($id,$data)
    {   
        $this->db->where("cust_id", $id);
        $this->db->update("customer", $data);  
    }

   
}