<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
    }
    public function store($data)
    {
        return $this->db->insert('cake',$data);
    }
    public function show()
    {
        $this->db->order_by('created_at', 'DESC');
        $query=$this->db->get('cake');  
        return $query->result();  
    }

   
}