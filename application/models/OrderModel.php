<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderModel extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
    }
    public function store($data)
    {
        return $this->db->insert('orderbake',$data);
    }
    public function show()
    {
        $this->db->select('*');
        $this->db->from('orderbake');
        $this->db->join('customer', 'customer.cust_id = orderbake.custID');
        $this->db->join('cake', 'cake.cake_id = orderbake.cakeID');
        $this->db->order_by('orderbake.created_at', 'DESC');

        $query=$this->db->get(); 
        return $query->result();  
    }
    public function get_customer()
    {
        $query=$this->db->get('customer');  
        return $query->result();  
    }
    public function get_cake()
    {
        $query=$this->db->get('cake');  
        return $query->result(); 
    }

   
}