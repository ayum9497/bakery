<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Cake Listing</span>
                <!-- <span class="section-heading-lower">We're Open</span> -->
              </h2>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Cake Name</th>
                                <th scope="col">Cake Description</th>
                                <th scope="col">Cake Price (RM)</th>
                                </tr>
                            </thead>
                            <?php if(count($cakes) > 0){?>
                            <?php $counter = 1; ?>
                            <tbody>
                            <?php foreach($cakes as $cake){?>
                                <tr>
                                <th scope="row"><?php echo $counter++; ?></th>
                                <td><?php echo $cake->cake_name; ?></td>
                                <td><?php echo $cake->cake_desc; ?></td>
                                <td><?php echo $cake->cake_price; ?></td>
                                </tr>
                            <?php }?>   
                            <?php }else{?>
                                <tr><td colspan="4"><p align="center">No Record founds !!</p></td></tr>
                            <?php }?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </section>

