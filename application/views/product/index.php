<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Create Cake</span>
                <!-- <span class="section-heading-lower">We're Open</span> -->
              </h2>

            <?php echo $this->session->flashdata('verify_msg'); ?>
             
             <?php $attributes = array("name" => "cakeCreateForm");
                        echo form_open("cake/create", $attributes);?>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="text" name="cakeName" class="form-control" id="cakeName" value="<?php echo set_value('cakeName'); ?>" placeholder="Enter Cake Name">
                        <span class="text-danger"><?php echo form_error('cakeName'); ?></span>
                    </div>
                </div>

                 <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="text" name="cakeDesc" class="form-control" id="cakeDesc" value="<?php echo set_value('cakeDesc'); ?>" placeholder="Enter Cake Description">
                        <span class="text-danger"><?php echo form_error('cakeDesc'); ?></span>
                    </div>
                </div>

                 <div class="form-row mb-4">
                    <div class="form-group col-md-12">
                        <input type="number" min="0.00" max="10000.00" step="0.01" name="cakePrice" class="form-control" id="cakePrice" value="<?php echo set_value('cakePrice'); ?>" placeholder="Enter Cake Price">
                        <span class="text-danger"><?php echo form_error('cakePrice'); ?></span>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                    </div>
                </div>
              <?php echo form_close(); ?>

            </div>
          </div>
        </div>
      </div>
    </section>
