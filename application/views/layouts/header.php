<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url();?>assets/img/cupcake.ico">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/business-casual.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3"><?php echo $title; ?></span>
      <span class="site-heading-lower">Business Casual</span>
    </h1> -->

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <!-- <li class="nav-item <?php echo strcmp($status,'home')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('/') ?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'about')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('about') ?>">About</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'product')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('product') ?>">Products</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'store')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('store') ?>">Store</a>
            </li> -->
            <li class="nav-item <?php echo strcmp($status,'customer')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('customer') ?>">Customer</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'customerlist')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('customer/list') ?>">Customer List</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'cake')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('cake') ?>">Cake</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'cakelist')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('cake/list') ?>">Cake List</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'order')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('order') ?>">Order</a>
            </li>
            <li class="nav-item <?php echo strcmp($status,'orderlist')=='0'?'active':''; ?> px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="<?= base_url('order/list') ?>">Order List</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>