<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Order List</span>
                <!-- <span class="section-heading-lower">We're Open</span> -->
              </h2>

                 <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Cake Ordered</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price (RM)</th>
                                </tr>
                            </thead>
                            <?php if(count($orders) > 0){?>
                            <?php $counter = 1; ?>
                            <tbody>
                            <?php foreach($orders as $order){?>
                                <tr>
                                <th scope="row"><?php echo $counter++; ?></th>
                                <td><?php echo $order->cust_name; ?></td>
                                <td><?php echo $order->cake_name; ?></td>
                                <td><?php echo $order->order_quan; ?></td>
                                <td><?php echo $order->cake_price."x".$order->order_quan."=".$order->cake_price*$order->order_quan; ?></td>
                                </tr>
                            <?php }?>   
                            <?php }else{?>
                                <tr><td colspan="5"><p align="center">No Record founds !!</p></td></tr>
                            <?php }?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </section>
