<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Create Order</span>
                <!-- <span class="section-heading-lower">We're Open</span> -->
              </h2>

            <?php echo $this->session->flashdata('verify_msg'); ?>
            
            <?php $attributes = array("name" => "orderCakeForm");
                        echo form_open("order/create", $attributes);?>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <select id="custID" name="custID" class="form-control">
                        <?php if(count($customers) > 0){ ?>
                            <option selected>Choose Customer...</option>
                            <?php foreach($customers as $cust){?>
                                <option value="<?php echo $cust->cust_id; ?>"><?php echo $cust->cust_name; ?></option>
                            <?php } ?>  
                        <?php }else{?>   
                            <option selected>No Customer</option>
                        <?php }?> 
                    </select>
                </div>
            </div>

             <div class="form-row">
                <div class="form-group col-md-12">
                    <select id="cakeID" name="cakeID" class="form-control">
                        <?php if(count($cakes) > 0){ ?>
                            <option selected>Choose Cake...</option>
                            <?php foreach($cakes as $cake){?>
                                <option value="<?php echo $cake->cake_id; ?>"><?php echo $cake->cake_name; ?></option>
                            <?php } ?>  
                        <?php }else{?>   
                            <option selected>No Cake</option>
                        <?php }?> 
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="number" min="1" max="999" step="1" name="orderQuant" class="form-control" id="orderQuant" value="<?php echo set_value('orderQuant'); ?>" placeholder="Enter Order Quantity">
                    <span class="text-danger"><?php echo form_error('orderQuant'); ?></span>
                </div>
            </div>
                            
            <div class="form-row mb-4">
                <div class="form-group col-md-12">
                    <input type="text" name="orderDesc" class="form-control" id="orderDesc" value="<?php echo set_value('orderDesc'); ?>" placeholder="Enter Order Description">
                    <span class="text-danger"><?php echo form_error('orderDesc'); ?></span>
                </div>
            </div>
                            
            <div class="form-row">
                <div class="form-group col-md-4 offset-md-4">
                <button type="submit" class="btn btn-primary btn-block">Create Order</button>
                </div>
            </div>

            <?php echo form_close(); ?>

            </div>
          </div>
        </div>
      </div>
    </section>
