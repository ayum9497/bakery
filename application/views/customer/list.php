<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Customer Listing</span>
                <!-- <span class="section-heading-lower">We're Open</span> -->
              </h2>

            <?php echo $this->session->flashdata('msg'); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <?php if(count($customers) > 0){?>
                            <?php $counter = 1; ?>
                            <tbody>
                            <?php foreach($customers as $cust){?>
                                <tr>
                                <th scope="row"><?php echo $counter++; ?></th>
                                <td><?php echo $cust->cust_name; ?></td>
                                <td><?php echo $cust->cust_phone; ?></td>
                                <td><?php echo $cust->cust_email; ?></td>
                                <td><button type="button" class="btn btn-success btn-sm try" id="<?php echo $cust->cust_id; ?>">Read</button>  <button type="button" class="btn btn-danger btn-sm remove" id="<?php echo $cust->cust_id; ?>">Delete</button> <button type="button" class="btn btn-warning btn-sm update" id="<?php echo $cust->cust_id; ?>">Update</button></td>
                                </tr>
                            <?php }?>   
                            <?php }else{?>
                                <tr><td colspan="5"><p align="center">No Record founds !!</p></td></tr>
                            <?php }?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </section>


<!-- show status -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="show-modal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="custName" placeholder="Customer Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="matricNo" class="col-sm-2 col-form-label">Mobile:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="phoneNumber" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lectEmail" class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="updated_at" class="col-sm-2 col-form-label">Address:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="address" placeholder="Address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="updated_at" class="col-sm-2 col-form-label">Created At:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="created_at" placeholder="created_at">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="updated_at" class="col-sm-2 col-form-label">Updated At:</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="updated_at" placeholder="updated_at">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- remove-modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="remove-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal form-remove" method="post">
                <div class="modal-body">
                    <strong>Are you sure want to remove !!</strong>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- update status -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="update-modal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal form-update" method="post">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name:</label>
                        <div class="col-sm-10">
                        <input type="text" name="cust_name" class="form-control" id="ucustName" placeholder="Customer Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="matricNo" class="col-sm-2 col-form-label">Mobile:</label>
                        <div class="col-sm-10">
                        <input type="text" name="cust_phone" class="form-control" id="uphoneNumber" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lectEmail" class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10">
                        <input type="text" name="cust_email" class="form-control" id="uemail" placeholder="Email">
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="updated_at" class="col-sm-2 col-form-label">Address:</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="uaddress" placeholder="Address">
                        </div>
                    </div> -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="uid">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript">
     // show
     $(document).on('click','.try', function(){
        var id = $(this).attr('id');
    
            $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>CustomerController/details',
            data: {id:id},
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#show-modal').modal('show');
                $('#custName').val(data[0].cust_name);
                $('#phoneNumber').val(data[0].cust_phone);
                $('#email').val(data[0].cust_email);
                $('#address').val(data.address);
                $('#created_at').val(data.created_at);
                $('#updated_at').val(data.updated_at);
            }
        });
    });
     // remove
     $(document).ready(function(){
        $(".remove").click(function(){
            
            var id = $(this).attr('id');

            console.log(id);
            $('#remove-modal').modal('show');
            $('#id').val(id);
            
        });
    });
    // remove submit
    $(document).on('submit','.form-remove', function(e){
        e.preventDefault();
        
            $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>CustomerController/delete',
            data: new FormData(this),
            contentType:false,  
            processData:false,
            success: function(data){
            
                console.log(data);
                $('#modal-remove').modal('hide');
                window.location.reload();
    
            }
        });
    });
     /* modal-update */
     $(document).on('click','.update', function(){
        var id = $(this).attr('id');
        
            $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>CustomerController/details',
            data: {id:id},
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#update-modal').modal('show');
                $('#uid').val(id);
                $('#ucustName').val(data[0].cust_name);
                $('#uphoneNumber').val(data[0].cust_phone);
                $('#uemail').val(data[0].cust_email);
                //$('#uaddress').val(data.address);
                //$('#ucreated_at').val(data.created_at);
                //$('#uupdated_at').val(data.updated_at);
            }
        });
    });
    /* update */
    $(document).on('submit','.form-update', function(e){
        e.preventDefault();
        
            $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>CustomerController/update',
            data: new FormData(this),
            contentType:false,  
            processData:false,
            success: function(data){
            
                console.log(data);
                $('#update-modal').modal('hide');
                window.location.reload();
    
            }
        });
    });
</script>

