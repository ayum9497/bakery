<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">

              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Create Account Customer</span>
                <!-- <span class="section-heading-lower">Customer Details</span> -->
              </h2>

             <?php echo $this->session->flashdata('verify_msg'); ?>
             
             <?php $attributes = array("name" => "custRegForm");
                        echo form_open("customer/register", $attributes);?>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo set_value('name'); ?>" placeholder="Enter Name">
                        <span class="text-danger"><?php echo form_error('name'); ?></span>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" name="phone" class="form-control" id="phone" value="<?php echo set_value('phone'); ?>" placeholder="Enter Phone Number">
                        <span class="text-danger"><?php echo form_error('phone'); ?></span>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" name="email" class="form-control" id="email" value="<?php echo set_value('email'); ?>" placeholder="Enter Email">
                        <span class="text-danger"><?php echo form_error('email'); ?></span>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="text" name="address" class="form-control" id="address" value="<?php echo set_value('address'); ?>" placeholder="Enter Address">
                        <span class="text-danger"><?php echo form_error('address'); ?></span>
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="form-group col-md-6">
                        <input type="text" name="city" class="form-control" id="city" value="<?php echo set_value('city'); ?>" placeholder="Enter City">
                        <span class="text-danger"><?php echo form_error('city'); ?></span>
                    </div>
                    <div class="form-group col-md-4">
                        <select id="state" name="state" class="form-control">
                            <option selected>Choose State...</option>
                            <option value="Johor">Johor</option>
                            <option value="Kedah">Kedah</option>
                            <option value="Kelantan">Kelantan</option>
                            <option value="Melaka">Melaka</option>
                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                            <option value="Pahang">Pahang</option>
                            <option value="Penang">Penang</option>
                            <option value="Perak">Perak</option>
                            <option value="Perlis">Perlis</option>
                            <option value="Sabah">Sabah</option>
                            <option value="Sarawak">Sarawak</option>
                            <option value="Selangor">Selangor</option>
                            <option value="Terengganu">Terengganu</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <input type="text" name="zipcode" class="form-control" id="zipcode" value="<?php echo set_value('zipcode'); ?>" placeholder="Zipcode" maxlength="5">
                        <span class="text-danger"><?php echo form_error('zipcode'); ?></span>
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="form-group col-md-4 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div>
                </div>
              <?php echo form_close(); ?>

 
            </div>
          </div>
        </div>
      </div>
    </section>