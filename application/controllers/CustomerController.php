<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerController extends CI_Controller {

    public function __construct() {
		
		parent::__construct();
		$this->load->helper(array('form','url','security'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->model('CustomerModel');
		date_default_timezone_set("Asia/Kuala_Lumpur");

	}
	public function index()
	{   
        $data['status'] = 'customer';
        $data['title'] = "Customers";

        $this->load->view('layouts/header', $data);
        $this->load->view('customer/index');
        $this->load->view('layouts/footer');
    }
    public function register()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[customer.cust_name]|min_length[3]|max_length[30]|xss_clean');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[customer.cust_email]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required|min_length[5]|max_length[5]');
        
        if ($this->form_validation->run() == FALSE)
        {
           $this->index();
          
        }else{

            $data = array(
				'cust_name' => $this->input->post('name'),
				'cust_phone' => $this->input->post('phone'),
				'cust_email' => $this->input->post('email'),
                'cust_address' => $this->input->post('address'),
                'cust_city' => $this->input->post('city'),
                'cust_state' => $this->input->post('state'),
                'cust_zip' => $this->input->post('zipcode'),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            );
            
            if($this->CustomerModel->store($data))
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        You are successfully registered!</div>');
                $this->index();
            }
            else
			{
				$this->session->set_flashdata('verify_msg','<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                Oops! Error. Please try again later!!!
		              </div>');
                $this->index();
			}
        }
    }
    public function list()
    {
        $data['status'] = 'customerlist';
        $data['title'] = "Customer Lists";
        $data['customers'] = $this->CustomerModel->show();
        
        $this->load->view('layouts/header', $data);
        $this->load->view('customer/list', $data);
        $this->load->view('layouts/footer');
    }
    public function details()
    {
        $cust_id = $this->input->post('id');  
        $data = $this->CustomerModel->details($cust_id);


        foreach($data as $i)
        {
            $data['address'] = $i->cust_address.' '.$i->cust_zip.' '.$i->cust_city.' '.$i->cust_state;
            $data['created_at'] = date("d/m/Y h:i A",strtotime($i->created_at));
            $data['updated_at'] = date("d/m/Y h:i A",strtotime($i->updated_at));
        }

        echo json_encode($data);
    }
    public function delete()
    {
        $cust_id = $this->input->post('id'); 
        $this->CustomerModel->delete($cust_id);

        $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Success Remove !!
            </div>');

    }
    public function update()
    {
        $cust_id = $this->input->post('id');
        $cust_name = $this->input->post('cust_name');
        $cust_email = $this->input->post('cust_email');
        $cust_phone = $this->input->post('cust_phone');
        
        $data = array(
            'cust_name' => $cust_name,
            'cust_phone' => $cust_phone,
            'cust_email' => $cust_email

        );

        $this->CustomerModel->update($cust_id,$data);

        $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Success Updated !!
            </div>');
    }

  
}
