<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public function __construct() {
		
		parent::__construct();
		//$this->load->library(array('session'));
		$this->load->helper(array('url'));
		//$this->load->model('Index/Model');
		date_default_timezone_set("Asia/Kuala_Lumpur");

	}
	public function home()
	{   
        $data['status'] = 'home';
        $data['title'] = "Welcome to Tersherah's Bakery";

        $this->load->view('layouts/header', $data);
        $this->load->view('home/home');
        $this->load->view('layouts/footer');
    }
    public function about()
    {
        $data['status'] = 'about';
        $data['title'] = 'About Us';

        $this->load->view('layouts/header', $data);
        $this->load->view('home/about');
        $this->load->view('layouts/footer');
    }
    public function product()
    {
         $data['status'] = 'product';
         $data['title'] = 'Products';

         $this->load->view('layouts/header', $data);
         $this->load->view('home/product');
         $this->load->view('layouts/footer');
    }
    public function store()
    {
        $data['status'] = 'store';
        $data['title'] = 'Store';

        $this->load->view('layouts/header', $data);
        $this->load->view('home/store');
        $this->load->view('layouts/footer');
    }
}
