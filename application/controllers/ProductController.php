<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller {

    public function __construct() {
		
		parent::__construct();
		$this->load->helper(array('form','url','security'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->model('ProductModel');
		date_default_timezone_set("Asia/Kuala_Lumpur");

	}
	public function index()
	{   
        $data['status'] = 'cake';
        $data['title'] = "Cake";

        $this->load->view('layouts/header', $data);
        $this->load->view('product/index');
        $this->load->view('layouts/footer');
    }
    public function create()
    {
        $this->form_validation->set_rules('cakeName', 'Cake Name', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('cakeDesc', 'Cake Description', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('cakePrice', 'Cake Price', 'trim|required|numeric');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->index();

        }else{
            
            $data = array(
				'cake_name' => $this->input->post('cakeName'),
				'cake_desc' => $this->input->post('cakeDesc'),
				'cake_price' => $this->input->post('cakePrice'),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            );

            if($this->ProductModel->store($data))
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Cake successfully created!</div>');
                $this->index();
            }else{
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                Oops! Error. Please try again later!!!
		              </div>');
                $this->index();
            }
        }
    }
    public function list()
    {
        $data['status'] = 'cakelist';
        $data['title'] = "Cake Lists";
        $data['cakes'] = $this->ProductModel->show();
        
        $this->load->view('layouts/header', $data);
        $this->load->view('product/list', $data);
        $this->load->view('layouts/footer');
    }
   
}
