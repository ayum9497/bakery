<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderController extends CI_Controller {

    public function __construct() {
		
		parent::__construct();
		$this->load->helper(array('form','url','security'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->model('OrderModel');
		date_default_timezone_set("Asia/Kuala_Lumpur");
	}
	public function index()
	{   
        $data['status'] = 'order';
        $data['title'] = "Order";

        $data['customers'] = $this->OrderModel->get_customer();
        $data['cakes'] = $this->OrderModel->get_cake();

        $this->load->view('layouts/header', $data);
        $this->load->view('order/index', $data);
        $this->load->view('layouts/footer');
    }
    public function create()
    {
        $this->form_validation->set_rules('custID', 'Customer', 'trim|required');
        $this->form_validation->set_rules('cakeID', 'Cake', 'trim|required');
        $this->form_validation->set_rules('orderQuant', 'Order Quantity', 'trim|required|numeric');
        $this->form_validation->set_rules('orderDesc', 'Order Description', 'trim|required|min_length[3]|max_length[30]|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $this->index();

        }else{

            $data = array(
				'order_quan' => $this->input->post('orderQuant'),
				'order_desc' => $this->input->post('orderDesc'),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'custID' => $this->input->post('custID'),
				'cakeID' => $this->input->post('cakeID')
            );

            if($this->OrderModel->store($data))
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Your Order successfully created!</div>');
                $this->index();
            }else{
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Oops! Error. Please try again later!!!
                    </div>');
                $this->index();
            }
        }
    }
    public function list()
    {
        $data['status'] = 'orderlist';
        $data['title'] = "Order List";
        $data['orders'] = $this->OrderModel->show();

        $this->load->view('layouts/header', $data);
        $this->load->view('order/list', $data);
        $this->load->view('layouts/footer');
    }
   
}
